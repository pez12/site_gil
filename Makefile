PAGES=$(wildcard pages/*.html)
ASSETS=$(wildcard assets/*)

all:: public/ $(subst pages/ ,public/ ,$(PAGES), $(ASSETS)) 

public/%: pages/% layout/before.html layout/after.html
	./scripts/buildpage.sh $<

public/%: assets/%
	mkdir -p public
	cp $< $@
	touch -m public/

clean:: public/
	rm -r $<

rebuild:: scripts/buildsite.sh
	./scripts/buildsite.sh

test:: 
	python3 -m http.server -d /public/

